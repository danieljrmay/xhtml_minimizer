<!-- markdownlint-disable MD033 -->
# <img src="doc/images/logo.png" width="100" alt="Logo"/> XHTML Minimizer

Minimise the size of XHTML files by:
* Removing comments.
* Removing the XML declaration.
* Replacing character and entity references with their equivilent UTF-8 character.
* Merging neibouring comments and cdata sections.
* Deleteing "meaningless" whitespace.

## Examples ##

Read `input.xhtml` and create a minimized version of it in `output.xhtml`.


```sh
xhtml-minimizer --output output.xhtml input.xhtml
```

Here we scan `input` directory looking for files of the form
`*.xhtml` which are minimized and saved in the `output` directory. We
can get a table of statistics about file size reductions by using the
`--verbose` flag.


```sh
xhtml-minimizer --verbose --output output input
             Input             |            Output             |      Reduction
-------------------------------------------------------------------------------------
        File        |   Size   |        File        |   Size   |    %      |   Size  
-------------------------------------------------------------------------------------
    index.xhtml     |  32131   |    index.xhtml     |  25555   |   20.5    |   6576  
      a.xhtml       |   3025   |      a.xhtml       |   2589   |   14.4    |   436   
      b.xhtml       |   8365   |      b.xhtml       |   7205   |   13.9    |   1160  

```
