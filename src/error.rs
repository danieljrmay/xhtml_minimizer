use std::fmt;

/// Errors which can be returned by the xhtml-minimizer main method.
pub enum Error {
    ThreadCount,
    StdInOutDir,
    OutExists,
    OutDirCreate,
    NoInput,
    InputPathDoesNotExist,
    InputIsDirOutputIsNotDir,
    InputIsDirOutputIsStdOut,
    InputIsFileOutputIsDir,
    ErrorReadingInputFileToString,
    NoOutput,
    DirInStdOut,
    IoError(std::io::Error),
    UnableToObtainFilename,
}
impl std::fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::ThreadCount => write!(f, "Unable to obtain thread count value."),
            Error::StdInOutDir => write!(
                f,
                "The output path must be a file (not a directory) when using standard input."
            ),
            Error::OutExists => write!(f, "The output file already exists."),
            Error::OutDirCreate => write!(f, "Failed to create the output directory."),
            Error::NoInput => write!(f, "No input has been provided."),
            Error::InputPathDoesNotExist => write!(f, "The input path does not exist."),
            Error::InputIsDirOutputIsNotDir => write!(
                f,
                "The output path must be a directory when the input is a directory."
            ),
            Error::InputIsDirOutputIsStdOut => write!(
                f,
                "Can not output to standard output when the input is a directory."
            ),
            Error::InputIsFileOutputIsDir => write!(
                f,
                "The output must be a file (not a directory) when the input is a file."
            ),
            Error::ErrorReadingInputFileToString => {
                write!(f, "There was an error reading the input file to a String.")
            }
            Error::NoOutput => write!(f, "No output has been specified."),
            Error::DirInStdOut => write!(
                f,
                "Can not write to standard output when the input is a directory."
            ),
            Error::IoError(io_error) => write!(f, "IO {}", io_error),
            Error::UnableToObtainFilename => write!(f, "Unable to obtain a filename from a path."),
        }
    }
}
impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", &self)
    }
}
impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Error::IoError(io_error) => Some(io_error),
            _ => None,
        }
    }
}
