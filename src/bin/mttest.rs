use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

fn main() {
    println!("A multi-threading example");

    let mut vec: Vec<usize> = Vec::new();
    for i in 1..1000 {
        vec.push(i)
    }

    let arc = Arc::new(Mutex::new(vec));

    let mut handles = Vec::new();

    for t in 1..10 {
        let t_arc = Arc::clone(&arc);

        let handle = thread::spawn(move || {
            println!("Started thread {}", t);

            loop {
                let mut mutex_guard = match t_arc.lock() {
                    Ok(mutex_guard) => mutex_guard,
                    Err(_mutex_guard) => {
                        panic!("Poison Error attempting to obtain the mutex guard");
                    }
                };

                let mut_ref_vec = &mut mutex_guard;
                let opt_value = mut_ref_vec.pop();
                drop(mutex_guard);

                match opt_value {
                    Some(v) => {
                        println!("Thread {} starting work on value {}", t, v);
                        thread::sleep(Duration::from_millis(100));
                        println!("Thread {} finished work on value {}", t, v);
                    }
                    None => break,
                }
            }
        });

        handles.push(handle);
    }

    for h in handles {
        h.join().unwrap()
    }
}
