use clap::ArgMatches;
use xhtml_minimizer::cli::clargs_parser;

/// Return an `ArgMatches` based on the specified command line `args`.
pub fn create_clargs(args: Vec<&str>) -> ArgMatches {
    let clargs_parser = clargs_parser();

    clargs_parser.get_matches_from(args)
}
