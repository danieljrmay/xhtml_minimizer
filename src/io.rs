use crate::cli::thread_count;
use crate::error::Error;
use crate::minimize_write;
use clap::ArgMatches;
use std::fmt;
use std::fs;
use std::fs::File;
use std::io::{BufWriter, Read, Write};
use std::path::Path;

/// The various IO configurations which can be specified by the
/// command line arguments.
#[derive(Debug)]
pub enum IoConfig<'a> {
    StdinStdout,
    StdinFileOut(&'a Path),
    FileInStdout(&'a Path),
    FileInFileOut(&'a Path, &'a Path),
    DirInDirOut(&'a Path, &'a Path),
}
impl<'a> IoConfig<'a> {
    pub fn new(clargs: &'a ArgMatches) -> Result<IoConfig<'a>, Error> {
        let opt_input_path = match clargs.value_of("input") {
            Some(input_filename) => Some(Path::new(input_filename)),
            None if clargs.is_present("stdin") => None,
            None => return Err(Error::NoInput),
        };

        let opt_output_path = match clargs.value_of("output") {
            Some(output_filename) => Some(Path::new(output_filename)),
            None if clargs.is_present("stdout") => None,
            None => return Err(Error::NoOutput),
        };

        match (opt_input_path, opt_output_path) {
            (Some(input_path), Some(output_path)) => {
                if input_path.is_dir() && output_path.is_dir() {
                    Ok(IoConfig::DirInDirOut(input_path, output_path))
                } else if input_path.is_dir() && !output_path.exists() {
                    match fs::create_dir_all(output_path) {
                        Ok(()) => Ok(IoConfig::DirInDirOut(input_path, output_path)),
                        Err(_) => Err(Error::OutExists),
                    }
                } else if input_path.is_file() && output_path.is_file() {
                    Ok(IoConfig::FileInFileOut(input_path, output_path))
                } else if input_path.is_file() && !output_path.exists() {
                    Ok(IoConfig::FileInFileOut(input_path, output_path))
                } else if !input_path.exists() {
                    Err(Error::InputPathDoesNotExist)
                } else if output_path.exists() {
                    Err(Error::OutExists)
                } else {
                    unreachable!()
                }
            }
            (Some(input_path), None) => {
                if input_path.is_file() {
                    Ok(IoConfig::FileInStdout(input_path))
                } else if input_path.is_dir() {
                    Err(Error::DirInStdOut)
                } else if !input_path.exists() {
                    Err(Error::InputPathDoesNotExist)
                } else {
                    unreachable!()
                }
            }
            (None, Some(output_path)) => {
                if !output_path.exists() {
                    Ok(IoConfig::StdinFileOut(output_path))
                } else if output_path.is_dir() {
                    Err(Error::StdInOutDir)
                } else if output_path.is_file() {
                    Err(Error::OutExists)
                } else {
                    unreachable!()
                }
            }
            (None, None) => Ok(IoConfig::StdinStdout),
        }
    }
}
impl<'a> fmt::Display for IoConfig<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            IoConfig::StdinStdout => {
                write!(f, "mode = stdin -> stdout\ninput = stdin\noutput = stdout")
            }
            IoConfig::StdinFileOut(output_path) => {
                write!(
                    f,
                    "mode = stdin -> file\ninput = stdin\noutput file = {}",
                    output_path.display()
                )
            }
            IoConfig::FileInStdout(input_path) => {
                write!(
                    f,
                    "mode = file -> stdout\ninput file = {}\noutput = stdout",
                    input_path.display()
                )
            }
            IoConfig::FileInFileOut(input_path, output_path) => {
                write!(
                    f,
                    "mode = file -> file\ninput file = {}\noutput file = {}",
                    input_path.display(),
                    output_path.display()
                )
            }
            IoConfig::DirInDirOut(input_path, output_path) => {
                write!(
                    f,
                    "mode = directory -> directory\ninput dir = {}\noutput dir = {}",
                    input_path.display(),
                    output_path.display()
                )
            }
        }
    }
}

/// Execute for stdin input, and stdout output.
pub fn stdin_input_stdout_output(clargs: &ArgMatches) -> Result<(), Error> {
    let mut input = String::new();

    if let Err(io_error) = std::io::stdin().read_to_string(&mut input) {
        return Err(Error::IoError(io_error));
    }

    if let Err(io_error) = minimize_write(
        input.chars(),
        &mut std::io::stdout().lock(),
        clargs.is_present("keep-comments"),
        clargs.is_present("keep-references"),
        clargs.is_present("keep-whitespace"),
        clargs.is_present("keep-xml-declaration"),
    ) {
        return Err(Error::IoError(io_error));
    }

    Ok(())
}

/// Execute for stdin input, and file output.
pub fn stdin_input_file_output(output_path: &Path, clargs: &ArgMatches) -> Result<(), Error> {
    let mut input = String::new();

    if let Err(io_error) = std::io::stdin().read_to_string(&mut input) {
        return Err(Error::IoError(io_error));
    }

    let output_file = match File::create(output_path) {
        Ok(f) => f,
        Err(io_error) => return Err(Error::IoError(io_error)),
    };

    let mut buffered_output = BufWriter::new(output_file);

    if let Err(io_error) = minimize_write(
        input.chars(),
        &mut buffered_output,
        clargs.is_present("keep-comments"),
        clargs.is_present("keep-references"),
        clargs.is_present("keep-whitespace"),
        clargs.is_present("keep-xml-declaration"),
    ) {
        return Err(Error::IoError(io_error));
    }

    if let Err(io_error) = buffered_output.flush() {
        return Err(Error::IoError(io_error));
    }

    Ok(())
}

/// Execute for file input, stdout output.
pub fn file_input_stdout_output(input_path: &Path, clargs: &ArgMatches) -> Result<(), Error> {
    let input = match fs::read_to_string(input_path) {
        Ok(s) => s,
        Err(_error) => return Err(Error::ErrorReadingInputFileToString),
    };

    if let Err(io_error) = minimize_write(
        input.chars(),
        &mut std::io::stdout().lock(),
        clargs.is_present("keep-comments"),
        clargs.is_present("keep-references"),
        clargs.is_present("keep-whitespace"),
        clargs.is_present("keep-xml-declaration"),
    ) {
        return Err(Error::IoError(io_error));
    }

    Ok(())
}

/// Execute for file input, file output.
pub fn file_input_file_output(
    input_path: &Path,
    output_path: &Path,
    clargs: &ArgMatches,
    print_header_and_footer: bool,
) -> Result<(), Error> {
    let input = match fs::read_to_string(input_path) {
        Ok(s) => s,
        Err(_error) => return Err(Error::ErrorReadingInputFileToString),
    };

    let output_file = match File::create(output_path) {
        Ok(f) => f,
        Err(io_error) => return Err(Error::IoError(io_error)),
    };

    let mut buffered_output = BufWriter::new(output_file);

    if let Err(io_error) = minimize_write(
        input.chars(),
        &mut buffered_output,
        clargs.is_present("keep-comments"),
        clargs.is_present("keep-references"),
        clargs.is_present("keep-whitespace"),
        clargs.is_present("keep-xml-declaration"),
    ) {
        return Err(Error::IoError(io_error));
    }

    if let Err(io_error) = buffered_output.flush() {
        return Err(Error::IoError(io_error));
    }

    if clargs.is_present("verbose") {
        if print_header_and_footer {
            print_file_size_statistics_table_header();
        }

        print_file_size_statistics_table_row(input_path, output_path)?;

        if print_header_and_footer {
            print_file_size_statistics_table_footer();
        }
    }

    Ok(())
}

/// Execute directory input, directory output.
pub fn dir_input_dir_output(
    input_dir_path: &Path,
    output_dir_path: &Path,
    clargs: &ArgMatches,
) -> Result<(), Error> {
    if clargs.is_present("verbose") {
        print_file_size_statistics_table_header();
    }

    let mut input_dir_iter = match input_dir_path.read_dir() {
        Ok(iter) => iter,
        Err(io_error) => return Err(Error::IoError(io_error)),
    };

    // Get the requested thread count
    let _thread_count = thread_count(clargs)?;
    // TODO implement multi-threaded execution.

    while let Some(Ok(dir_entry)) = input_dir_iter.next() {
        let input_path_buf = dir_entry.path();
        let input_path = input_path_buf.as_path();

        if !input_path.is_file() || input_path.extension().unwrap() != "xhtml" {
            continue;
        }

        let mut output_path_buf = output_dir_path.to_path_buf();
        output_path_buf.push(input_path.file_name().unwrap());
        let output_path = output_path_buf.as_path();

        if output_path.exists() {
            return Err(Error::OutExists);
        }

        file_input_file_output(input_path, output_path, clargs, false)?;
    }

    if clargs.is_present("verbose") {
        print_file_size_statistics_table_footer();
    }

    Ok(())
}

/// Print the header row of the file size statistics table.
fn print_file_size_statistics_table_header() {
    println!("|{:-<85}|", "-");
    println!("|{: ^31}|{: ^31}|{: ^21}|", "Input", "Output", "Reduction");
    println!("|{:-<85}|", "-");
    println!(
        "|{: ^20}|{: ^10}|{: ^20}|{: ^10}|{: ^10}|{: ^10}|",
        "File", "Size", "File", "Size", "%", "Size"
    );
    println!("|{:-<85}|", "-");
}

/// Print the footer row of the file size statistics table.
fn print_file_size_statistics_table_footer() {
    println!("|{:-<85}|", "-");
}

/// Print a data row of the file size statistics table.
fn print_file_size_statistics_table_row(
    input_path: &Path,
    output_path: &Path,
) -> Result<(), Error> {
    let input_filename = match input_path.file_name() {
        Some(os_str) => match os_str.to_str() {
            Some(s) => s,
            None => return Err(Error::UnableToObtainFilename),
        },
        None => return Err(Error::UnableToObtainFilename),
    };

    let input_size = match fs::metadata(input_path) {
        Ok(metadata) => metadata.len(),
        Err(io_error) => return Err(Error::IoError(io_error)),
    };

    let output_filename = match output_path.file_name() {
        Some(os_str) => match os_str.to_str() {
            Some(s) => s,
            None => return Err(Error::UnableToObtainFilename),
        },
        None => return Err(Error::UnableToObtainFilename),
    };

    let output_size = match fs::metadata(output_path) {
        Ok(metadata) => metadata.len(),
        Err(io_error) => return Err(Error::IoError(io_error)),
    };

    let reduction = input_size - output_size;

    let percent_reduction = format!("{:.1}", (reduction as f64) / (input_size as f64) * 100.0);

    println!(
        "|{: ^20}|{: ^10}|{: ^20}|{: ^10}|{: ^10}|{: ^10}|",
        input_filename, input_size, output_filename, output_size, percent_reduction, reduction
    );

    Ok(())
}
