extern crate clap;
extern crate num_cpus;

use xhtml_minimizer::cli::{clargs_parser, thread_count};
use xhtml_minimizer::error::Error;
use xhtml_minimizer::io::*;

/// Entry point
pub fn main() -> Result<(), Error> {
    let clargs = clargs_parser().get_matches();

    // Print debugging information about the parsed command line
    // arguments if requested.
    if clargs.is_present("debug") {
        println!(
            "Command line arguments:\n\
             keep-comments = {}\n\
             keep-references = {}\n\
             keep-whitespace = {}\n\
             keep-xml-declaration = {}\n\
             verbose = {}\n\
             output = {:?}\n\
             stdin = {}\n\
             stdout = {}\n\
             threads = {:?}\n\
             input = {:?}",
            clargs.is_present("keep-comments"),
            clargs.is_present("keep-references"),
            clargs.is_present("keep-whitespace"),
            clargs.is_present("keep-xml-declaration"),
            clargs.is_present("verbose"),
            clargs.value_of("output"),
            clargs.is_present("stdin"),
            clargs.is_present("stdout"),
            clargs.value_of("threads"),
            clargs.value_of("input")
        );
    }

    // Create an IO configuration from the provided command line
    // arguments.
    let io_config = IoConfig::new(&clargs)?;

    // Print debugging information about the IO configuration if
    // reqeuested.
    if clargs.is_present("debug") {
        println!("\nIO configuration:\n{}", io_config);
        println!("thread count = {}", thread_count(&clargs)?);
    }

    // Dispatch execution according to the `io_config` varient.
    match io_config {
        IoConfig::StdinStdout => stdin_input_stdout_output(&clargs),
        IoConfig::StdinFileOut(output_path) => stdin_input_file_output(output_path, &clargs),
        IoConfig::FileInStdout(input_path) => file_input_stdout_output(input_path, &clargs),
        IoConfig::FileInFileOut(input_path, output_path) => {
            file_input_file_output(input_path, output_path, &clargs, true)
        }

        IoConfig::DirInDirOut(input_path, output_path) => {
            dir_input_dir_output(input_path, output_path, &clargs)
        }
    }
}
