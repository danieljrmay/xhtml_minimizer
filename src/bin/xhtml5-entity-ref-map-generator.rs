//! This program is used to generate the *some* of the source code
//! which makes up the `Xhtml5EntityRefMap` decode function.
//!
//! It converts the `utils/entities.json` file, in root of [this
//! projects
//! repository](https://gitlab.com/danieljrmay/xhtml_minimizer), to a
//! fragment of rust source code. The latest version of
//! `entities.json` can be obtained from
//! [https://html.spec.whatwg.org/entities.json](https://html.spec.whatwg.org/entities.json).
//!
//! Run the script and capture the output with `cargo run --bin
//! xhtml5-entity-ref-map-generator > source-code-fragment.txt`, you
//! can then copy and paste the fragment into the rust source file.

use serde_json::Value;
use std::collections::BTreeMap;
use std::fs::File;

const ENTITIES_JSON_FILE: &str = "utils/entities.json";

/// Entry point for this program.
fn main() -> Result<(), Error> {
    let input_file = match File::open(ENTITIES_JSON_FILE) {
        Ok(f) => f,
        Err(io_error) => return Err(Error::Io(io_error)),
    };

    let entities: Value = match serde_json::from_reader(input_file) {
        Ok(v) => v,
        Err(serde_error) => return Err(Error::Serde(serde_error)),
    };

    let map = match entities {
        Value::Object(map) => map,
        _ => return Err(Error::NoMatchingMap),
    };

    let mut sorted_map: BTreeMap<String, String> = BTreeMap::new();

    for (entity_markup, value) in map {
        let prefix_stripped_entity_name = match entity_markup.strip_prefix('&') {
            Some(psn) => psn,
            None => return Err(Error::MissingEntityNamePrefix),
        };

        let entity_name = match prefix_stripped_entity_name.strip_suffix(';') {
            Some(ssn) => ssn.to_owned(),
            None => continue,
        };

        let value_map = match value {
            Value::Object(value_map) => value_map,
            _ => return Err(Error::NoMatchingMap),
        };

        let entity_value = match value_map.get("characters") {
            Some(Value::String(s)) => s.to_owned(),
            _ => return Err(Error::NoMatchingString),
        };

        sorted_map.insert(entity_name, entity_value);
    }

    for (name, value) in sorted_map {
        println!("\"{}\" => Ok(String::from({:?})),", name, value);
    }

    Ok(())
}

/// The errors which can be returned by this program.
#[derive(Debug)]
enum Error {
    Io(std::io::Error),
    Serde(serde_json::Error),
    NoMatchingMap,
    MissingEntityNamePrefix,
    NoMatchingString,
}
impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Error: {:?}", self)
    }
}
impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Error::Io(io_error) => Some(io_error),
            Error::Serde(serde_error) => Some(serde_error),
            _ => None,
        }
    }
}
