use crate::error::Error;
use clap::{Arg, ArgMatches, Command};

/// Return the clap command line argument parser.
pub fn clargs_parser() -> Command<'static> {
    // Define the command line argument parser.
    Command::new("XHTML Minimizer")
        .version("0.2.0")
        .author("Daniel J. R. May")
        .about("Minimise the size of XHTML files.")
        .arg(
            Arg::new("keep-comments")
                .short('c')
                .long("keep-comments")
                .takes_value(false)
                .help("Do not remove comments"),
        )
        .arg(
            Arg::new("keep-references")
                .short('r')
                .long("keep-references")
                .takes_value(false)
                .help("Do not replace entity and character references"),
        )
        .arg(
            Arg::new("keep-whitespace")
                .short('w')
                .long("keep-whitespace")
                .takes_value(false)
                .help("Keep all whitespace"),
        )
        .arg(
            Arg::new("keep-xml-declaration")
                .short('x')
                .long("keep-xml-declaration")
                .takes_value(false)
                .help("Keep any XML declaration"),
        )
        .arg(
            Arg::new("debug")
                .short('d')
                .long("debug")
                .takes_value(false)
                .help("Print messages which are useful for debugging"),
        )
        .arg(
            Arg::new("verbose")
                .short('v')
                .long("verbose")
                .takes_value(false)
                .help("Print statistics about file size reduction"),
        )
        .arg(
            Arg::new("output")
                .short('o')
                .long("output")
                .takes_value(true)
                .required_unless_present("stdout")
                .help("The output file or directory"),
        )
        .arg(
            Arg::new("stdin")
                .short('s')
                .long("stdin")
                .takes_value(false)
                .help("Get input from standard input"),
        )
        .arg(
            Arg::new("stdout")
                .short('u')
                .long("stdout")
                .takes_value(false)
                .conflicts_with_all(&["output", "verbose", "debug"])
                .help("Send output to standard output"),
        )
        .arg(
            Arg::new("threads")
                .short('t')
                .long("threads")
                .takes_value(true)
                .default_value("1")
                .validator(threads_clarg_validator)
                .help("Maximum number of execution threads"),
        )
        .arg(
            Arg::new("input")
                .conflicts_with_all(&["help", "version", "stdin"])
                .help("The input XHTML file or directory"),
        )
}

/// Validator for the `--threads` command line argument.
fn threads_clarg_validator(value: &str) -> Result<(), String> {
    let mut core_count: usize = num_cpus::get();

    // Sometimes containers return 0 for `core_count`! So we force
    // `core_count = 1` so that this validator passes the default
    // value of 1.
    if core_count < 1 {
        core_count = 1;
    }

    match value.parse::<usize>() {
        Ok(v) if v >= 1 && v <= core_count => Ok(()),
        _ => Err(format!(
            "<threads> must be an integer between 1 and {}.",
            core_count
        )),
    }
}

/// Return the thread count as a `usize` if able to parse it as such
/// from the command line arguments.
pub fn thread_count(clargs: &ArgMatches) -> Result<usize, Error> {
    if let Some(s) = clargs.value_of("threads") {
        if let Ok(thread_count) = s.parse::<usize>() {
            return Ok(thread_count);
        }
    }

    Err(Error::ThreadCount)
}
