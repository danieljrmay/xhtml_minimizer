mod common;

use file_diff::diff;
use std::fs;
use std::path::Path;
use xhtml_minimizer::io::dir_input_dir_output;
use xhtml_minimizer::io::file_input_file_output;

#[test]
fn test_file_input_file_output() {
    let input_path = Path::new("tests/input/a.xhtml");
    let output_dir = Path::new("tests/output/test_file_input_file_output");
    let output_path = output_dir.join("a.xhtml");

    let clargs = common::create_clargs(vec![
        "xhtml-minimizer",
        "--debug",
        "--verbose",
        "--threads",
        "1",
        "--output",
        output_path.to_str().unwrap(),
        input_path.to_str().unwrap(),
    ]);

    if output_dir.is_dir() {
        assert!(fs::remove_dir_all(&output_dir).is_ok());
    }
    assert!(fs::create_dir_all(&output_dir).is_ok());

    assert!(!output_path.exists());
    assert!(file_input_file_output(input_path, &output_path, &clargs, true).is_ok());
    assert!(output_path.is_file());

    let expected_path = "tests/expected/a.xhtml";

    assert!(diff(expected_path, output_path.as_path().to_str().unwrap()));
}

#[test]
fn test_dir_input_dir_output() {
    let input_dir = Path::new("tests/input");
    let output_dir = Path::new("tests/output/test_dir_input_dir_output");
    let output_path_a = output_dir.join("a.xhtml");
    let output_path_b = output_dir.join("b.xhtml");
    let output_path_c = output_dir.join("c.xhtml");

    let clargs = common::create_clargs(vec![
        "xhtml-minimizer",
        "--debug",
        "--verbose",
        "--threads",
        "1",
        "--output",
        output_dir.to_str().unwrap(),
        input_dir.to_str().unwrap(),
    ]);

    if output_dir.is_dir() {
        assert!(fs::remove_dir_all(&output_dir).is_ok());
    }

    assert!(fs::create_dir_all(&output_dir).is_ok());

    assert!(dir_input_dir_output(input_dir, output_dir, &clargs).is_ok());

    assert!(output_path_a.is_file());
    assert!(output_path_b.is_file());
    assert!(output_path_c.is_file());

    let expected_path_a = "tests/expected/a.xhtml";
    let expected_path_b = "tests/expected/b.xhtml";
    let expected_path_c = "tests/expected/c.xhtml";

    assert!(diff(
        expected_path_a,
        output_path_a.as_path().to_str().unwrap()
    ));

    assert!(diff(
        expected_path_b,
        output_path_b.as_path().to_str().unwrap()
    ));

    assert!(diff(
        expected_path_c,
        output_path_c.as_path().to_str().unwrap()
    ));
}
