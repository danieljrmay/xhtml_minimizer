# XHTML Minimizer Todos #

* [x] Add all entity references.
* [x] Add debug flag.
* [x] Change verbose flag to output stats.
* [x] Add CI.
* [x] Automatically create output directory if does not exist.
* [x] Refactor error out of `xhtml-minimizer` to facilitate testing.
* [x] Refactor io out of `xhtml-minimizer` to facilitate testing.
* [x] Add test XHTML files.
* [x] Add expected result tests against XHTML files and fix bugs.
* [x] Normalize whitespace in attribute values as per [Attribute value
      whitespace
      handling](https://www.liquid-technologies.com/XML/Attribute.aspx)
* [ ] Add multi-threaded implementation when in directory mode.
* [ ] Add brotli compression see https://github.com/dropbox/rust-brotli
* [ ] Improve `README.md`.
* [ ] Suggest some improvements to `file_diff` crate.
* [ ] Add more tests.
* [ ] Release.
* [ ] Improve table column alignments.
* [ ] Improve error messages and exit status codes. See
      https://www.joshmcguigan.com/blog/custom-exit-status-codes-rust/
      and https://docs.rs/exitcode/latest/exitcode/
* [ ] Update entity generation binary to produce enitre module file.
